# picrust2 Singularity container
### Package picrust2 Version 2.5.1

Container for PICRUSt2 v2.5.1

PICRUSt2 (Phylogenetic Investigation of Communities by Reconstruction of Unobserved States) is a software for predicting functional abundances based only on marker gene sequences

Homepage:

https://huttenhower.sph.harvard.edu/picrust/

Github:

https://github.com/picrust/picrust2

Wiki:

https://github.com/picrust/picrust2/wiki

Package installation using Miniconda3 V4.12.0 python 3.7

All packages/scripts are in /opt/miniconda/bin & are in PATH

Default runscript: picrust2_pipeline.py

Usage:

./picrust2_v2.5.1.sif --help

singularity exec picrust2_v2.5.1.sif picrust2_pipeline.py --help



Local build:
```
sudo singularity build picrust2_v2.5.1.sif Singularity_picrust2_v2.5.1.def
```

Get image help:
```
singularity run-help picrust2_v2.5.1.sif
```

Default runscript: picrust2_pipeline.py<br>
Usage:
```
./picrust2_v2.5.1.sif --help
```
or:
```
singularity exec picrust2_v2.5.1.sif picrust2_pipeline.py --help
```

image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>
can be pull (singularity version >=3.3) with:<br>

```
singularity pull picrust2_v2.5.1.sif oras://registry.forgemia.inra.fr/gafl/singularity/picrust2/picrust2:latest

```

